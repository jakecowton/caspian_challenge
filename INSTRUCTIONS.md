# Caspian code challenge instructions

Your challenge is to provide code for obtaining transaction categories using a given source.

## Timescales
* We don’t expect you to have to dedicate more than a couple hours to this. 

## Delivery
* We have provided a README in which you can explain any significant design decisions you’ve made or questions you had about the requirements.
* We would prefer you to deliver your solution as a zip file or link to your repo. If that presents a problem for any reason, just let us know. 
 
## Code 

The solution is split into train and prediction stages. Train stage produces the model to be used in the prediction stage.

In the code provided you'll find the method definitions for you to implement:

### Train

```py
trainer = TransactionCategoryTrainer()
trainer.train(transactions)
```
* The method definition (`train`) should not be changed. 
* This method must produce a transaction category model file to be used by `TransactionCategoryEstimator`.
* If this method is unable to train, it should raise an exception.

### Inference

```py
estimator = TransactionCategoryEstimator()
estimator.get_category(transaction)
```

* The method definition (`get_category`) should not be changed. 
* This method must return the category (Entertainment, Groceries, Personal) for a single transaction. 
* If this method is unable to estimate a transaction category, it should raise an exception.

To implement the method definition per the requirements please create any new files, classes, methods, etc. that you feel necessary for a good design.

## Data source
In the data directory we've provided a file with UK transaction information plus labels. You should use this as your source of transactions for your solution. 

## Solution Requirements
Do not spend time modeling the estimator we are interested in the engineering decisions taken to solve per the requirements. There is no credit for techniques used to model this so keep it as simple as possible.

* Must be written in Python 3.6 or later.
* We want to know the transaction category (Entertainment, Groceries, Personal) for a given account transaction history from a UK account. 
* We want to be able to run the training stage and inference/prediction stage separately.  
* In the future we will want to support more countries and their transaction data. We would like you to design your solution with this in mind. 
  

## New to Python?

Download python 3.7 or later from here https://www.python.org/downloads/

To install Python packages go here https://pip.pypa.io/en/stable/quickstart/

## Getting Started

We've included failing test files to get you started.
The tests use `unittest` (https://docs.python.org/3/library/unittest.html) but you're welcome to use another test framework you're more comfortable with. 

You can use the following steps to run the solution.
We'll use the same steps when we come to test it, you can add any additional steps in the README.md.

1. ``pip install -r requirements.txt.``

2. ``python -m pytest tests/``

3. We should also be able to use Python shell to run the methods provided.
