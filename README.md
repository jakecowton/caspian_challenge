
# Caspian coding challenge

Thank you for your interest in Caspian. This template is a barebones guide to get you started. Please add any libs, folders, files etc you see fit in order to produce a solution you're proud of.

# Instructions

Please see INSTRUCTIONS.md for more information.

# Setup and Run Instructions

```bash
git clone https://gitlab.com/jakecowton/caspian_challenge.git
cd caspian_challenge
mkvirtualenv -p python3.8 caspian-challenge # Any virtual environment for python3.8 will do fine
pip install -r requirements.txt
python -m pytest tests
python demo.py
```

## Design Decisions

I endeavoured to make the trainer/estimator classes as decoupled from the dataset as possible, so the codebase for each is incredibly simple, most of the heavy lifting is done in the `TransactionDataSet` class.
Functions prefixed with `_` indicate that they should be treated as private functions.

The trainer can be initialized as any scikit-learn model (the default is a `RandomForestClassifier`), with configurations passed using `**kwargs`. The data types of parameters for the other functions is defined by "type hints" and additional information is provided in the docstring.

The estimator requires a trained model (or a path to one that can be loaded using `pickle.load`) and an ID to class label dictionary that can convert a class ID produced by the model into a class label.
The `get_category()` function takes in a 1*M array and returns a class label.
I would have preferred for it to return a class ID as this would make it fully decoupled from the dataset, however the specification said this method needed to return the class label, so it has been included at this level.

The `DataSet` class outlines a base class structure for a DataSet that accepts a CSV file to load from using Pandas.
It also requests the columns to be used as inputs to the model and the target label column, this gives flexibility for adding additional fields to use for training; I have suggested some defaults in `demo.py`.
ID to class and class to ID dictionaries are created upon instantiation and a `mode` parameter allows for defining if the data is train or test data.

For data preprocessing, input, target and test data undergo a base level of preprocessing to format the data correctly, however these should be overridden in subclasses as I have done for `TransactionDataSet`.
This class inherits from `DataSet` and implements dataset specific preprocessing such as translating non-numeric data into category codes.
This is an incredibly simple approach to codifying any text data.
The main complexity here is that if test data contains a category that did not exist in the train data, a new category needs to be created and codified.

Regarding error handling, as the specification said problems with training/inference should raise exceptions, I have left them outside of try/except/finally loops, as the exceptions raised by scikit-learn are generally very informative.

## Questions

One of the requirements for the estimator is, "If this method is unable to estimate a transaction category, it should raise an exception". I interpreted this as if it cannot make a classification at all. I realise you might have meant that if it couldn't make a reasonable classification an error should be raised. In which case I would modify this to check class probabilities first using something like `RandomForestClassifier.predict_log_proba()`.