import numpy as np
import pandas

from src.dataset import TransactionDataSet

def test_dataset_loading():
    d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label")

    assert type(d.df) == pandas.DataFrame

def test_getting_training_data():
    d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label")
    training_data = d.training_data
    assert type(training_data) == tuple

    inputs, targets = training_data
    assert type(inputs) == np.ndarray
    assert type(targets) == np.ndarray