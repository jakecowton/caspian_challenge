import os
import random

import pytest

from src.dataset import TransactionDataSet
from src.transaction_category_trainer import TransactionCategoryTrainer
from src.transaction_category_estimator import TransactionCategoryEstimator

def setup_trainer():
    d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label")
    t = TransactionCategoryTrainer()
    model = t.train(d)
    return d, t, model

def test_fails_without_id_to_class():
    _, _, model = setup_trainer()
    with pytest.raises(TypeError):
        e = TransactionCategoryEstimator(model=model)

def test_returns_category():
    d, t, model = setup_trainer()

    d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label",
                        mode="test")

    e = TransactionCategoryEstimator(model=model,
                                     id_to_class_dict=d.id_to_class)

    for row, label in d.test_data:
        cat = e.get_category(row)
        assert type(cat) == str
        assert cat in d.df.Label.unique()

def test_loading_model():
    d,t, _ = setup_trainer()
    t.save_model("a_model.pkl")
    e = TransactionCategoryEstimator(model="a_model.pkl",
                                     id_to_class_dict=d.id_to_class)
    os.remove("a_model.pkl")
