import os

import sklearn

from src.dataset import TransactionDataSet
from src.transaction_category_trainer import TransactionCategoryTrainer


def test_produces_transaction_category_model():
    d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label")
    t = TransactionCategoryTrainer()
    model = t.train(d)
    assert type(model) == sklearn.ensemble.RandomForestClassifier

def test_model_save():
    d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label")
    t = TransactionCategoryTrainer()
    t.train(d)
    t.save_model("a_model.pkl")
    assert os.path.exists("a_model.pkl")
    os.remove("a_model.pkl")