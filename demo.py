import random

import numpy as np

from src.dataset import TransactionDataSet
from src.transaction_category_trainer import TransactionCategoryTrainer as Trainer
from src.transaction_category_estimator import TransactionCategoryEstimator as Estimator

from sklearn.metrics import f1_score

np.random.seed(66666)

def main():
    train_d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label")

    test_d = TransactionDataSet("./data/uk_transactions.csv",
                        input_cols=["Amount", "PayedTo"],
                        target_col="Label",
                        mode="test")

    # Divide the dataset into train and test sets
    train_rows = np.random.choice(train_d.df.index.values, 100)
    test_rows = [i for i in range(len(train_d.df)) if i not in train_rows]
    train_d.df = train_d.df.iloc[train_rows]
    test_d.df = test_d.df.iloc[test_rows]

    t = Trainer(n_estimators=10, n_jobs=4)

    model = t.train(train_d)
    # Can also save the model using
    # t.save_model("model.pkl")

    e = Estimator(model=model, id_to_class_dict=test_d.id_to_class)
    # Can load another model using
    # e.load_model("model.pkl")

    # Print the estimations for the test set
    y_pred = []
    y_true = []
    for transaction, label in test_d.test_data:
        pred = e.get_category(transaction)
        y_pred.append(pred)
        y_true.append(label)
        print("Pred: ", pred, " | Real: ", label)

    print("F1 Score: ", f1_score(y_true, y_pred, average="weighted"))

if __name__ == "__main__":
    main()
