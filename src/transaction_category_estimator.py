import pickle

import numpy as np

from src.dataset import DataSet


class TransactionCategoryEstimator:

    def __init__(self, model, id_to_class_dict: dict):
        """
        model: Either a sklearn model that his been fitted to training data
               or a path to a file to load one from
        id_to_class_dict: a dictionary showing how to translate IDs output
                          from the model into labels
        """
        self.id_to_class_dict = id_to_class_dict
        if type(model) == str:
            self.classifier = self._load_model(model)
        else:
            self.classifier = model

    def _load_model(self, model_path: str):
        """
        model_path: a path a to a pickled model
        """
        return pickle.load(open(model_path, 'rb'))

    def get_category(self, transaction: np.ndarray):
        """
        transaction: a 1*M numpy array containing the inputs to classify,
                     where M is the number inputs in the model
        id_to_class_dict
        """
        assert isinstance(transaction, np.ndarray)
        assert transaction.shape[0] == 1, "Only pass one set of inputs for prediction"

        class_id = self.classifier.predict(transaction)[0]
        return self.id_to_class_dict[class_id]
