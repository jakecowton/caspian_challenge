import pickle

from src.dataset import DataSet
from sklearn.ensemble import RandomForestClassifier


class TransactionCategoryTrainer:

    def __init__(self, model=RandomForestClassifier, **kwargs):
        """
        Builds and trains a classifier
        **kwargs: the params for model (see the model docs on scikit-learn)
        """
        self.classifier = RandomForestClassifier(**kwargs)

    def train(self, transactions: DataSet):
        """
        transactions: a DataSet to train on, src.dataset.DataSet

        returns: a fitted scikit-learn model
        """
        assert isinstance(transactions, DataSet)

        inputs, targets = transactions.training_data
        self.classifier.fit(inputs, targets)
        return self.classifier

    def save_model(self, filepath: str):
        pickle.dump(self.classifier, open(filepath, 'wb'))