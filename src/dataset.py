import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None


class DataSet(object):
    """
    Base class for a supported Dataset
    """

    def __init__(self, csv_filepath: str, input_cols: list, target_col: str,
                 mode="train"):
        """
        csv_filepath: a path to the CSV to load using pandas
        input_cols: the columns from the dataframe to be used as inputs to the model
        output_cols: the column from the DataFrame that contains the target class
        mode: either 'train' or 'test', determines access to the `test_data` property
        """
        self.df = self._read_from_csv(csv_filepath)
        self.input_cols = input_cols
        self.target_col = target_col
        self.mode = mode

        self.id_to_class = self._id_to_class()
        self.class_to_id = self._class_to_id()

    @property
    def training_data(self):
        """
        returns: tuple of two numpy arrays of shapes ((n * m), (n))
        """
        return self.inputs, self.targets

    @property
    def inputs(self):
        """Get the inputs for training a model"""
        return self._preprocess_inputs(self.df)

    @property
    def targets(self):
        """Get the targets for model training"""
        return self._preprocess_targets(self.df)

    @property
    def test_data(self):
        """
        returns: an iterator to containing the inputs and target for test data
                 if DataSet's mode is set to "test"
        """
        if self.mode == "train":
            raise TypeError("This is training dataset, not test, see `self.mode`")
        else:
            for _, row in self.df.iterrows():
                inputs, target = self._preprocess_prediction_data(row)
                yield inputs, target

    def _preprocess_inputs(self, df):
        return df.get(input_cols).values

    def _preprocess_targets(self, df):
        return df[self.target_col].values

    def _preprocess_prediction_data(self, row):
        return row

    def _id_to_class(self):
        classes = sorted(self.df.get(self.target_col).unique())
        return {i: c for i, c in enumerate(classes)}

    def _class_to_id(self):
        classes = sorted(self.df.get(self.target_col).unique())
        return {c: i for i, c in enumerate(classes)}

    def _read_from_csv(self, transactions_filepath):
        return pd.read_csv(transactions_filepath)


class TransactionDataSet(DataSet):
    """A specific instance of DataSet for the transaction data"""

    category_codes = {}

    def _to_category_code(self, series):
        series_category = series.astype("category")
        categories = series_category.cat.categories
        self.category_codes[series.name] = dict(zip(categories,
                                                range(len(categories))))
        return series_category.cat.codes

    def _preprocess_inputs(self, df):
        inputs = df.get(self.input_cols)
        for col in inputs.columns:
            if inputs[col].dtype == "O": # Check if it's an "object" type
                inputs[col] = self._to_category_code(inputs[col])
        return inputs.values

    def _preprocess_targets(self, df):
        targets = df.get(self.target_col)
        target_list = [self.class_to_id.get(class_name) for class_name in targets]
        return np.array(target_list)

    def _preprocess_prediction_data(self, series):
        label = series.get(self.target_col)
        series = series.get(self.input_cols)
        for col in series.index:
            if col in self.category_codes.keys():
                try:
                    series[col] = self.category_codes.get(col)[series[col]]
                except KeyError:
                    # Not a category that has been seen before
                    current_codes = list(self.category_codes[col].values())
                    new_category_code = max(list(current_codes)) + 1
                    self.category_codes[col][new_category_code] = series[col]
                    series[col] = new_category_code
        return np.array([series.values]), label
